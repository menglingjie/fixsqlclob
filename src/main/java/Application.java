import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.insert.Insert;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) throws JSQLParserException {
        try (
                Stream<String> lines = Files.lines(Paths.get("c:/tmp/input.sql"));
                FileWriter fw = new FileWriter("c:/tmp/output.sql")
        ) {
            StringBuffer sb = new StringBuffer();
            lines.forEach(line->{
                sb.append(line);
                if(line.endsWith(");")){
                    try {
                        String newString = cutLongString(sb.toString());
                        fw.write(newString);
                        sb.delete(0,sb.length());
                    } catch (JSQLParserException | IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    sb.append("\n");
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String cutLongString(String sqlString) throws JSQLParserException {
        CCJSqlParserManager pm = new CCJSqlParserManager();
        Statement statement = pm.parse(new StringReader(sqlString));
        final int maxLength = 2000;//字符串超过4000，会报“ORA-01704:字符串文字太长”错误，这里保险期间，只要超过2000就拆分字符串
        int columnNumber = 5;//定义要修改的超长字符串的列索引
        if (statement instanceof Insert) {
            Insert insertStatement = (Insert) statement;
            ExpressionList expressionList = (ExpressionList) insertStatement.getItemsList();
            Expression expression = (expressionList.getExpressions().get(columnNumber));
            if (expression instanceof StringValue) {
                StringValue noteExpression = (StringValue) expression;
                String noteString = noteExpression.getValue();
                if (noteString.length() < maxLength) {
                    return sqlString + "\n";
                }
                //走到这里，说明字符串超过maxLength了。
                Concat concatExpression = new Concat();
                //按照https://stackoverflow.com/a/66978000/4081628里提到的对策，添加函数empty_clob()
                concatExpression.withLeftExpression(new Function().withName("empty_clob"));

                for (int i = 0; i < (noteString.length()+maxLength-1) /maxLength; i++) {
                    int endIndex = Math.min(noteString.length(),(i + 1) *maxLength);
                    String shortString = noteString.substring(i *maxLength, endIndex);
                    if(concatExpression.getRightExpression()!=null){
                        Concat newConcatExpression = new Concat();
                        newConcatExpression.withLeftExpression(concatExpression);
                        concatExpression = newConcatExpression;
                    }
                    //拼接拆分后的字符串
                    concatExpression.withRightExpression(new StringValue(shortString));
                }
                expressionList.getExpressions().set(columnNumber, concatExpression);
                return insertStatement.toString() + ";\n";
            }else{
                return sqlString + "\n";
            }
        }else{
            System.out.println("不支持的指令");
            return "";
        }
    }
}
